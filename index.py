from tkinter import *
from tkinter import ttk
import tkinter.messagebox as tmsg
import os
import time


menu_category = ["Tea & Coffee","Beverages","Fast Food","Starters","Main Course","Dessert"]

menu_category_dict = {
    "Tea & Coffee":"1 Tea & Coffee.txt",
    "Beverages":"2 Beverages.txt",
    "Fast Food":"3 Fast Food.txt",
    "Starters":"4 Starters.txt",
    "Main Course":"5 Main Course.txt",
    "Dessert":"6 Dessert.txt"
}

order_dict = {}
for i in menu_category:
    order_dict[i] = {}

os.chdir(os.path.dirname(os.path.abspath(__file__)))

#- Charger le produits
def load_menu():
    menuCategory.set("")
    menu_tabel.delete(*menu_tabel.get_children())
    menu_file_list = os.listdir("Menu")
    for file in menu_file_list:
        f = open("Menu\\" + file , "r")
        category=""
        while True:
            line = f.readline()
            if(line==""):
                menu_tabel.insert('',END,values=["","",""])
                break
            elif (line=="\n"):
                continue
            elif(line[0]=='#'):
                category = line[1:-1]
                name = "\t\t"+line[:-1]
                price = ""
            elif(line[0]=='*'):
                name = line[:-1]
                price = ""
            else:
                name = line[:line.rfind(" ")]
                price = line[line.rfind(" ")+1:-3]
            menu_tabel.insert('',END,values=[name,price,category])

#Charger la table de commande du client       
def load_order():
    order_tabel.delete(*order_tabel.get_children())
    for category in order_dict.keys():
        if order_dict[category]:
            for lis in order_dict[category].values():
                order_tabel.insert('',END,values=lis)
    update_total_price()

#- Fonction d'ajout d'une commande
def add_button_operation():
    name = itemName.get()
    rate = itemRate.get()
    category = itemCategory.get()
    quantity = itemQuantity.get()

    if name in order_dict[category].keys():
        tmsg.showinfo("Error", "Item already exist in your order (L'article existe déjà dans votre commande)")
        return
    if not quantity.isdigit():
        tmsg.showinfo("Error", "Please Enter Valid Quantity (Veuillez entrer une quantité valide : valeur numerique)")
        return
    lis = [name,rate,quantity,str(int(rate)*int(quantity)),category]
    order_dict[category][name] = lis
    load_order()

#- Recuperer et Charge la commande dans l'option de validation, modification et Rejet des commandes
def load_item_from_menu(event):
    cursor_row = menu_tabel.focus()
    contents = menu_tabel.item(cursor_row)
    row = contents["values"]

    itemName.set(row[0])
    itemRate.set(row[1])
    itemCategory.set(row[2])
    itemQuantity.set("1")

#- Recuperer une commande du client deja dans ces commandes passé
def load_item_from_order(event):
    cursor_row = order_tabel.focus()
    contents = order_tabel.item(cursor_row)
    row = contents["values"]

    itemName.set(row[0])
    itemRate.set(row[1])
    itemQuantity.set(row[2])
    itemCategory.set(row[4])

#- Rechercher une categorie
def show_button_operation():
    category = menuCategory.get()
    if category not in menu_category:
        tmsg.showinfo("Error", "Please select valid Choice (Veuillez sélectionner un choix valide)")
    else:
        menu_tabel.delete(*menu_tabel.get_children())
        f = open("Menu\\" + menu_category_dict[category] , "r")
        while True:
            line = f.readline()
            if(line==""):
                break
            if (line[0]=='#' or line=="\n"):
                continue
            if(line[0]=='*'):
                name = "\t"+line[:-1]
                menu_tabel.insert('',END,values=[name,"",""])
            else:
                name = line[:line.rfind(" ")]
                price = line[line.rfind(" ")+1:-3]
                menu_tabel.insert('',END,values=[name,price,category])

#- Nettoyer l'espace de commande
def clear_button_operation():
    itemName.set("")
    itemRate.set("")
    itemQuantity.set("")
    itemCategory.set("")

#- Nettoyer l'espace de log des commandes (pannier)
def cancel_button_operation():
    names = []
    for i in menu_category:
        names.extend(list(order_dict[i].keys()))
    if len(names)==0:
        tmsg.showinfo("Error", "Your order list is Empty")
        return
    ans = tmsg.askquestion("Cancel Order", "Are You Sure to Cancel Order?")
    if ans=="no":
        return
    order_tabel.delete(*order_tabel.get_children())
    for i in menu_category:
        order_dict[i] = {}
    clear_button_operation()
    update_total_price()

#- Fonction de modification d'une commande
def update_button_operation():
    name = itemName.get()
    rate = itemRate.get()
    category = itemCategory.get()
    quantity = itemQuantity.get()

    if category=="":
        return
    if name not in order_dict[category].keys():
        tmsg.showinfo("Error", "Item is not in your order list (L'article n'est pas dans votre liste de commandes)")
        return
    if order_dict[category][name][2]==quantity:
        tmsg.showinfo("Error", "No changes in Quantity (Pas de changement de quantité)")
        return
    order_dict[category][name][2] = quantity
    order_dict[category][name][3] = str(int(rate)*int(quantity))
    load_order()

#- Fonction de rejet d'une commande
def remove_button_operation():
    name = itemName.get()
    category = itemCategory.get()

    if category=="":
        return
    if name not in order_dict[category].keys():
        tmsg.showinfo("Error", "Item is not in your order list (L'article n'est pas dans votre liste de commandes)")
        return
    del order_dict[category][name]
    load_order()

#- Gestion de la somme totale à payer
def update_total_price():
    price = 0
    for i in menu_category:
        for j in order_dict[i].keys():
            price += int(order_dict[i][j][3])
    if price == 0:
        totalPrice.set("")
    else:
        totalPrice.set(str(price)+" fcfa")

def bill_button_operation():
    customer_name = customerName.get()
    customer_contact = customerContact.get()
    names = []
    for i in menu_category:
        names.extend(list(order_dict[i].keys()))
    if len(names)==0:
        tmsg.showinfo("Error", "Your order list is Empty (Votre liste de commandes est vide)")
        return
    if customer_name=="" or customer_contact=="":
        tmsg.showinfo("Error", "Customer Details Required (Détails du client requis)")
        return
    if not customerContact.get().isdigit():
        tmsg.showinfo("Error", "Invalid Customer Contact (Contact client invalide)")
        return   
    ans = tmsg.askquestion("Generate Bill", "Are You Sure to Generate Bill? (Êtes-vous sûr de vouloir générer la facture ?)")
    ans = "yes"
    #DLQ1231
    #CODE.NO:- 34VASFD2215HKAAA
    if ans=="yes":
        bill = Toplevel()
        screen_center(app=bill, title="Bill", w=650, h=500, resiz=False)
        bill_text_area = Text(bill, font=("Courier New", 12))
        st = "\t\t\tBILLING\n\t- DLQ1231\n"
        st += f"\t- CODE.NO : 23ENSPYHN3CIA{time.time()}\n\n\n"
        st += "*"*64 + "\n" + "*"*64 + "\nDate: "
        
        t = time.localtime(time.time())
        week_day_dict = {0:"Monday",1:"Tuesday",2:"Wednesday",3:"Thursday",4:"Friday",5:"Saturday",6:"Sunday"}
        mday, mon, year, hour, min, sec = t.tm_mday, t.tm_mon, t.tm_year, t.tm_hour, t.tm_min, t.tm_sec
        st += f"{mday}/{mon}/{year} : ({week_day_dict[t.tm_wday]})"
        st += " "*10 + f"\t\t\t\tTime: {hour}:{min}:{sec}"
        st += f"\nCustomer Name: {customer_name}\nCustomer Contact: {customer_contact}\n\n"
         
        st += "*"*64 + "\n" + "*"*64 + "\nDESCRIPTION\t\t\tRATE\t\tQUANTITY\t\tAMOUNT\n"
        st += "-"*64 + "\n"
        
        for i in menu_category:
            for j in order_dict[i].keys():
                lis = order_dict[i][j]
                name = lis[0]
                rate = lis[1]
                quantity = lis[2]
                price = lis[3]
                st += name + "\t\t\t" + rate + "\t\t " + quantity + "\t\t  " + price + "\n"
        
        st += "\n\n" + "*"*64 + "\n" + "*"*64 + "\n\n\n"
        st += f"\t- TOTAL PRICE : {totalPrice.get()}\n"
        #st += "-"*130
 
        bill_text_area.insert(1.0, st)

        folder = f"{mday}-{mon}-{year}"
        if not os.path.exists(f"BillBDD\\{folder}"):
            os.makedirs(f"BillBDD\\{folder}")
        file_name = str(customer_name)+"_"+str(customer_contact)+"_"+str(mday)+"_"+str(mon)+"_"+str(year)+"_"+str(hour)+"_"+str(min)+"_"+str(sec)
        file = open(f"BillBDD\\{folder}\\{file_name}.txt", "w")
        file.write(st)
        file.close()

        
        order_tabel.delete(*order_tabel.get_children())
        for i in menu_category:
            order_dict[i] = {}
        clear_button_operation()
        update_total_price()
        customerName.set("")
        customerContact.set("")

        bill_text_area.pack(expand=True, fill=BOTH)
        #bill.focus_set()
        #bill.protocol("WM_DELETE_WINDOW", close_window)

#def close_window():
#    tmsg.showinfo("Thanks", "Thanks for using our service")
#    root.destroy()

#- Parametre de fenetre
def screen_center(app, title, w, h, resiz):
    app.title(title)
    app.iconbitmap("logo.ico")
    app.minsize(w, h)
    screenX, screenY = int(app.winfo_screenwidth()), int(app.winfo_screenheight())
    windowsX, windowsY = w, h
    app.resizable(width = resiz, height = resiz)
    app.minsize(windowsX, windowsY)
    posX, posY = ((screenX - windowsX) // 2), ((screenY - windowsY) // 2)
    geometry = "{}x{}+{}+{}".format(windowsX, windowsY, posX, posY)
    app.geometry(geometry)

root = Tk()
screen_center(app=root, title="Billing App", w=1400, h=835, resiz=True)

style_button = ttk.Style()
style_button.configure("TButton",font = ("Courier New", 10, "bold"), background="Blue")

title_frame = Frame(root, bd=3, bg="lightblue", relief=GROOVE)
title_frame.pack(side=TOP, fill=X)
logo = PhotoImage(file="logo.png")
title_logo = Label(title_frame, image=logo)
title_logo.pack(side=RIGHT, padx=20)
#title_logo.grid(row = 0, column = 0, padx=200)
title_label = Label(title_frame, text="Billing management", font=("Courier New", 35, "bold"),bg = "lightblue", fg="blue", pady=5)
title_label.pack(side=LEFT, padx=200)
#title_label.grid(row = 0, column = 1, padx=0)

customer_frame = LabelFrame(root,text="Customer Details",font=("Courier New", 8, "bold"), bd=4, bg="lightblue", relief=GROOVE)
customer_frame.pack(side=TOP, fill=X)

customer_name_label = Label(customer_frame, text="Name", font=("Courier New", 15, "bold"),bg = "lightblue", fg="black")
customer_name_label.grid(row = 0, column = 0)
customerName = StringVar()
customerName.set("")
customer_name_entry = Entry(customer_frame,width=40,font=("Courier New", 15, "bold"),bd=2, textvariable=customerName)
customer_name_entry.grid(row = 0, column=1,padx=50, pady=5)

customer_contact_label = Label(customer_frame, text="Contact", font=("Courier New", 15, "bold"),bg = "lightblue", fg="black")
customer_contact_label.grid(row = 0, column = 2)
customerContact = StringVar()
customerContact.set("")
customer_contact_entry = Entry(customer_frame,width=20,font=("Courier New", 15, "bold"),bd=2, textvariable=customerContact)
customer_contact_entry.grid(row = 0, column=3,padx=50, pady=5)

#- Selection des produits
menu_frame = Frame(root,bd=5, bg="lightblue", relief=GROOVE)
menu_frame.place(x=750,y=135,height=700,width=650)

menu_label = Label(menu_frame, text="Menu", font=("Courier New", 30, "bold"),bg = "lightblue", fg="Green", pady=0)
menu_label.pack(side=TOP,fill=X)

menu_category_frame = Frame(menu_frame,bg="lightblue",pady=10)
menu_category_frame.pack(fill=X)
combo_lable = Label(menu_category_frame,text="Select Categorie", font=("Courier New", 15, "bold"),bg = "lightblue", fg="black")
combo_lable.grid(row=0,column=0,padx=5)
menuCategory = StringVar()
combo_menu = ttk.Combobox(menu_category_frame,values=menu_category, textvariable=menuCategory)
combo_menu.grid(row=0,column=1,padx=10)
show_button = ttk.Button(menu_category_frame, text="Show",width=10,command=show_button_operation)
show_button.grid(row=0,column=2,padx=40)
show_all_button = ttk.Button(menu_category_frame, text="Show All", width=10,command=load_menu)
show_all_button.grid(row=0,column=3)

menu_tabel_frame = Frame(menu_frame)
menu_tabel_frame.pack(fill=BOTH,expand=1)

scrollbar_menu_x = Scrollbar(menu_tabel_frame,orient=HORIZONTAL)
scrollbar_menu_y = Scrollbar(menu_tabel_frame,orient=VERTICAL)

style = ttk.Style()
style.configure("Treeview.Heading",font=("Courier New",16, "bold"), fg="blue")
style.configure("Treeview",font=("Courier New", 15, "bold"),rowheight=40)

menu_tabel = ttk.Treeview(menu_tabel_frame,style = "Treeview", columns =("name","price","category"),xscrollcommand=scrollbar_menu_x.set, yscrollcommand=scrollbar_menu_y.set)
menu_tabel.heading("name",text="Product")
menu_tabel.heading("price",text="Rate")
menu_tabel["displaycolumns"]=("name", "price")
menu_tabel["show"] = "headings"
menu_tabel.column("price",width=50,anchor='center')

scrollbar_menu_x.pack(side=BOTTOM,fill=X)
scrollbar_menu_y.pack(side=RIGHT,fill=Y)
scrollbar_menu_x.configure(command=menu_tabel.xview)
scrollbar_menu_y.configure(command=menu_tabel.yview)
menu_tabel.pack(fill=BOTH,expand=1)

load_menu()
menu_tabel.bind("<ButtonRelease-1>",load_item_from_menu)

#- Validation, Modification, Rejet du choix de la commande
item_frame = Frame(root,bd=5, bg="lightblue", relief=GROOVE)
item_frame.place(x=0,y=135,height=250,width=750)

item_title_label = Label(item_frame, text="Item", font=("Courier New", 30, "bold"),bg = "lightblue", fg="Green")
item_title_label.pack(side=TOP,fill=X)

item_frame2 = Frame(item_frame, bg="lightblue")
item_frame2.pack(fill=X)

item_name_label = Label(item_frame2, text="Product", font=("Courier New", 15, "bold"),bg = "lightblue", fg="black")
item_name_label.grid(row=0,column=0, pady=10)
itemCategory = StringVar()
itemCategory.set("")
itemName = StringVar()
itemName.set("")
item_name = Entry(item_frame2, font=("Courier New", 15, "bold"),textvariable=itemName,state=DISABLED, width=25)
item_name.grid(row=0,column=1,padx=5, pady=10)

item_rate_label = Label(item_frame2, text="Price", font=("Courier New", 15, "bold"),bg = "lightblue", fg="black")
item_rate_label.grid(row=0,column=2,padx=30, pady=10)
itemRate = StringVar()
itemRate.set("")
item_rate = Entry(item_frame2, font=("Courier New", 15, "bold"),textvariable=itemRate,state=DISABLED, width=8)
item_rate.grid(row=0,column=3,padx=10, pady=10)

item_quantity_label = Label(item_frame2, text="Quantity", font=("Courier New", 15, "bold"),bg = "lightblue", fg="black")
item_quantity_label.grid(row=1,column=0,padx=30,pady=15)
itemQuantity = StringVar()
itemQuantity.set("")
item_quantity = Entry(item_frame2, font=("Courier New", 15, "bold"),textvariable=itemQuantity, width=5)
item_quantity.grid(row=1,column=1)

#- Ajouter une commande
item_frame3 = Frame(item_frame, bg="lightblue")
item_frame3.pack(fill=X)
add_button = ttk.Button(item_frame3, text="Add", command=add_button_operation)
add_button.grid(row=0,column=0,padx=40,pady=30)
#- Retirer une commande
remove_button = ttk.Button(item_frame3, text="Remove",command=remove_button_operation)
remove_button.grid(row=0,column=1,padx=40,pady=30)
#- Modifier une commande
update_button = ttk.Button(item_frame3, text="Update",command=update_button_operation)
update_button.grid(row=0,column=2,padx=40,pady=30)
#- Tout effacer
clear_button = ttk.Button(item_frame3, text="Clear",width=8,command=clear_button_operation)
clear_button.grid(row=0,column=3,padx=40,pady=30)

#- Pannier
order_frame = Frame(root,bd=5, bg="lightblue", relief=GROOVE)
order_frame.place(x=0,y=383,height=450,width=750)

order_title_label = Label(order_frame, text="Your Order", font=("Courier New", 30, "bold"),bg = "lightblue", fg="Green")
order_title_label.pack(side=TOP,fill=X)

order_tabel_frame = Frame(order_frame)
order_tabel_frame.place(x=0,y=50,height=340,width=740)
scrollbar_order_x = Scrollbar(order_tabel_frame,orient=HORIZONTAL)
scrollbar_order_y = Scrollbar(order_tabel_frame,orient=VERTICAL)
order_tabel = ttk.Treeview(order_tabel_frame,columns =("name","rate","quantity","price","category"),xscrollcommand=scrollbar_order_x.set,yscrollcommand=scrollbar_order_y.set)
order_tabel.heading("name",text="Product")
order_tabel.heading("rate",text="Rate")
order_tabel.heading("quantity",text="Quantity")
order_tabel.heading("price",text="Price")
order_tabel["displaycolumns"]=("name", "rate","quantity","price")
order_tabel["show"] = "headings"
order_tabel.column("rate",width=100,anchor='center', stretch=NO)
order_tabel.column("quantity",width=100,anchor='center', stretch=NO)
order_tabel.column("price",width=100,anchor='center', stretch=NO)
order_tabel.bind("<ButtonRelease-1>",load_item_from_order)

scrollbar_order_x.pack(side=BOTTOM,fill=X)
scrollbar_order_y.pack(side=RIGHT,fill=Y)
scrollbar_order_x.configure(command=order_tabel.xview)
scrollbar_order_y.configure(command=order_tabel.yview)
order_tabel.pack(fill=BOTH,expand=1)


total_price_label = Label(order_frame, text="Total Price", font=("Courier New", 15, "bold"),bg = "lightblue", fg="black")
total_price_label.pack(side=LEFT,anchor=SW,padx=20,pady=10)
totalPrice = StringVar()
totalPrice.set("")
total_price_entry = Entry(order_frame, font="arial 12",textvariable=totalPrice,state=DISABLED, width=10)
total_price_entry.pack(side=LEFT,anchor=SW,padx=0,pady=10)
bill_button = ttk.Button(order_frame, text="Bill",width=8, command=bill_button_operation)
bill_button.pack(side=LEFT,anchor=SW,padx=80,pady=10)
cancel_button = ttk.Button(order_frame, text="Cancel",command=cancel_button_operation)
cancel_button.pack(side=LEFT,anchor=SW,padx=20,pady=10)

root.mainloop()

